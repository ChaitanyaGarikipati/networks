package networks.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;
import networks.service.inf.Action;
import networks.service.inf.DeviceFactory;

public class AddDevice extends Action {
	Device device;
	
	@Override
	public boolean validateInput() {
		this.device = DeviceFactory.getDevice(this.args);
		if (this.device == null) {
			System.out.println(ErrorConstants.INVALID_SYNTAX);
			return false;
		}
		return true;
	}

	@Override
	public void executeCommand() {
		if (this.networkgraph.containsKey(this.device)) {
			System.out.println(ErrorConstants.NAME_EXISTS);
		} else {
			this.networkgraph.put(this.device, new ArrayList<Device>());
			System.out.println(MessageFormat.format(ApplicationConstants.ADDEED_SUCCESS, this.args[2]));
		}
	}

}