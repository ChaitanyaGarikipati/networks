package networks.service.impl;

import java.util.List;
import java.util.Map.Entry;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;
import networks.service.inf.Action;
import networks.utils.MapUtils;

public class ConnectDevices extends Action {

	@Override
	public void executeCommand() {
		Device d1 = new Device(this.args[1]);
		Device d2 = new Device(this.args[2]);

		if (!this.networkgraph.containsKey(d1) || !this.networkgraph.containsKey(d2)) {
			System.out.println(ErrorConstants.NODE_NOT_FOUND);
			return;
		}
		
		if (d1.hashCode() == d2.hashCode()) {
			System.out.println(ErrorConstants.CONNECT_TO_ITSELF);
			return;
		}
		
		if (this.networkgraph.get(d1).contains(d2)) {
			System.out.println(ErrorConstants.ALREADY_CONNECTED);
			return;
		}
		Entry<Device, List<Device>> d1Entry = MapUtils.getEntry(this.networkgraph, d1);
		Entry<Device, List<Device>> d2Entry = MapUtils.getEntry(this.networkgraph, d2);
		
		this.networkgraph.get(d1Entry.getKey()).add(d2Entry.getKey());
		this.networkgraph.get(d2Entry.getKey()).add(d1Entry.getKey());
		System.out.println(ApplicationConstants.CONNECTED_SUCCESS);
	}

}