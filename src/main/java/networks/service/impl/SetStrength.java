package networks.service.impl;

import java.util.List;
import java.util.Map.Entry;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;
import networks.service.inf.Action;
import networks.utils.MapUtils;

public class SetStrength extends Action {
	
	@Override
	public boolean validateInput() {
		if (!super.validateInput() || !this.args[2].chars().allMatch(Character::isDigit)) {
			System.out.println(ErrorConstants.INVALID_SYNTAX);
			return false;
		}
		return true;
	}

	@Override
	public void executeCommand() {
		Device device = new Device(this.args[1]);
		int strength = Integer.valueOf(this.args[2]);
		if (!this.networkgraph.containsKey(device)) {
			System.out.println(ErrorConstants.NODE_NOT_FOUND);
		} else {
			Entry<Device, List<Device>> dEntry = MapUtils.getEntry(this.networkgraph, device);
			if (ApplicationConstants.REPEATER.equals(dEntry.getKey().getType())) {
				System.out.println(ErrorConstants.INVALID_SYNTAX);
				return;
			}
			dEntry.getKey().setStrength(strength);
			System.out.println(ApplicationConstants.STRENGTH_SUCCESS);
		}
	}

}