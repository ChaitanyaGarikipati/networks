package networks.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;
import networks.service.inf.Action;
import networks.utils.MapUtils;

public class Send extends Action {

	@Override
	public boolean validateInput() {
		if (args.length == 4) {
			return true;
		} else {
			System.out.println(ErrorConstants.INVALID_SYNTAX);
			return false;
		}
	}

	@Override
	public void executeCommand() {
		String msg = this.args[3];
		Device d1 = new Device(this.args[1]);
		Device d2 = new Device(this.args[2]);

		if (!this.networkgraph.containsKey(d1) || !this.networkgraph.containsKey(d2)) {
			System.out.println(ErrorConstants.NODE_NOT_FOUND);
			return;
		}

		if (d1.equals(d2)) {
			System.out.println(d1.getName() + ApplicationConstants.ROUTE_CONNECTOR + d2.getName());
		} else {
			Entry<Device, List<Device>> d1Entry = MapUtils.getEntry(this.networkgraph, d1);
			Entry<Device, List<Device>> d2Entry = MapUtils.getEntry(this.networkgraph, d2);
			d1 = d1Entry.getKey();
			d2 = d2Entry.getKey();
			if (ApplicationConstants.REPEATER.equals(d1.getType())
					|| ApplicationConstants.REPEATER.equals(d2.getType())) {
				System.out.println(ErrorConstants.ROUTE_TO_REPEATER);
				return;
			}
			Set<String> visitingLedger = new HashSet<String>();
			visitingLedger.add(d1.getName());
			List<Device> devices = new ArrayList<Device>();
			devices.add(d1);
			devices = MapUtils.findRoute(this.networkgraph, d1, d2, visitingLedger, d1.getStrength(), devices);

			if (devices.size() > 1) {
				String processedMsg = msg;
				for (Device device : devices) {
					processedMsg = device.processMsg(processedMsg);
				}
				System.out.println(processedMsg);
			} else {
				System.out.println(ErrorConstants.ROUTE_NOT_FOUND);
			}
		}
	}

}
