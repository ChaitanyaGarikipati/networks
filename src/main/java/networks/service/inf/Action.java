package networks.service.inf;

import java.util.List;
import java.util.Map;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;

public abstract class Action {
	protected Map<Device, List<Device>> networkgraph = null;
	protected String[] args;
	
	public void parseInput(final String cmd) {
		this.args = cmd.split(ApplicationConstants.SPACE_STRING);
	};

	public abstract void executeCommand();
	
	public boolean validateInput() {
		if (args.length == 3) {
			return true;
		} else {
			System.out.println(ErrorConstants.INVALID_SYNTAX);
			return false;
		}
	}

	public void execute(final String args, Map<Device, List<Device>> networkgraph) {
		this.networkgraph = networkgraph;
		parseInput(args);
		
		if (validateInput()) {	
			executeCommand();
		}
	}
}
