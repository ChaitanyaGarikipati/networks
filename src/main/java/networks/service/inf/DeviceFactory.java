package networks.service.inf;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.devices.Bridge;
import networks.devices.Computer;
import networks.devices.Repeater;

public class DeviceFactory {
	public static Device getDevice(String[] args) {
		Device device = null;
		String deviceType = args[1];
		switch (deviceType) {
		case ApplicationConstants.COMPUTER:
			device = new Computer(args[2]);
			break;
		case ApplicationConstants.REPEATER:
			device = new Repeater(args[2]);
			break;
		case ApplicationConstants.BRIDGE:
			if (Bridge.validateInput(args)) {
				device = new Bridge(args[2], args[3]);
			}
			break;
		}
		return device;
	}
}
