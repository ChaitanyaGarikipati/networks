package networks.devices;

import networks.beans.Device;
import networks.constants.ApplicationConstants;

public class Computer extends Device {

	public Computer(String name) {
		super(name, ApplicationConstants.COMPUTER);
	}

	@Override
	public int updateStrength(int strength) {
		return strength-1;
	}
}
