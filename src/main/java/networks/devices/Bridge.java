package networks.devices;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ErrorConstants;

public class Bridge extends Device {
	private String bridgeType;

	public Bridge(String name) {
		super(name, ApplicationConstants.BRIDGE);
	}
	
	public Bridge(String name, String bridgeType) {
		super(name, ApplicationConstants.BRIDGE);
		this.bridgeType = bridgeType;
	}

	@Override
	public int updateStrength(int strength) {
		return strength - 2;
	}
	
	@Override
	public String processMsg(String msg) {
		if (ApplicationConstants.UPPER.equals(bridgeType)) {
			msg = msg.toUpperCase();
		} else if (ApplicationConstants.LOWER.equals(bridgeType)) {
			msg = msg.toLowerCase();
		}
		return msg;
	}
	
	public static boolean validateInput(String[] args) {
		if (args.length == 4 && (ApplicationConstants.UPPER.equals(args[3]) || ApplicationConstants.LOWER.equals(args[3]))) {
			return true;
		} else {
			System.out.println(ErrorConstants.INVALID_SYNTAX);
			return false;
		}
	}
}
