package networks.devices;

import networks.beans.Device;
import networks.constants.ApplicationConstants;

public class Repeater extends Device {

	public Repeater(String name) {
		super(name, ApplicationConstants.REPEATER);
	}

	@Override
	public int updateStrength(int strength) {
		return strength*2;
	}
}
