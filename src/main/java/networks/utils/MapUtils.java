package networks.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import networks.beans.Device;
import networks.constants.ErrorConstants;

public class MapUtils {
	public static Entry<Device, List<Device>> getEntry(Map<Device, List<Device>> graph, Device device) {
		for (Entry<Device, List<Device>> entry : graph.entrySet()) {
			if (entry.getKey().equals(device)) {
				return entry;
			}
		}
		throw new RuntimeException(ErrorConstants.MAP_ENTRY_NOT_FOUND);
	}

	public static List<Device> findRoute(Map<Device, List<Device>> graph, Device start, Device end,
			Set<String> visitingLedger, int msgSrtg, List<Device> devicePath) {
		List<Device> result = new ArrayList<Device>();
		if (start.equals(end)) {
			return devicePath;
		}
		for (Device d : graph.get(start)) {
			if (!visitingLedger.contains(d.getName())) {
				msgSrtg = d.updateStrength(msgSrtg);
				if (msgSrtg < 0) {
					return new ArrayList<Device>();
				}
				visitingLedger.add(d.getName());
				devicePath.add(d);
				result.addAll(findRoute(graph, d, end, visitingLedger, msgSrtg, devicePath));
			}
		}

		return result;
	}
}
