package networks.constants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ApplicationConstants {
	public static final String EMPTY_STRING = "";
	public static final String SPACE_STRING = " ";
	public static final String SUCCESS_MSG_BASE = "Successfully ";
	public static final String COMPUTER = "COMPUTER";
	public static final String REPEATER = "REPEATER";
	public static final String BRIDGE = "BRIDGE";
	public static final String UPPER = "UPPER";
	public static final String LOWER = "LOWER";
	public static final String ROUTE_CONNECTOR = " -> ";
	public static final String ADDEED_SUCCESS = SUCCESS_MSG_BASE + "added {0}.";
	public static final String STRENGTH_SUCCESS = SUCCESS_MSG_BASE + "defined strength.";
	public static final String CONNECTED_SUCCESS = SUCCESS_MSG_BASE + "connected.";
	
	public static final HashSet<String> DEVICE_TYPE = new HashSet<String>();

	public static enum COMMAND_ACTION {
		ADD("ADD"), SET_STRENGTH("SET_DEVICE_STRENGTH"), CONNECT("CONNECT"), FIND_ROUTE("INFO_ROUTE"), SEND_MSG("SEND"),
		INVALID_COMMAND("INVALID_COMMAND");

		String cmd;

		private COMMAND_ACTION(String cmdName) {
			this.cmd = cmdName;
		}

		public String getCmd() {
			return this.cmd;
		}
	}
	
	public final static Map<String, COMMAND_ACTION> ACTION_MAPPINGS = new HashMap<String, ApplicationConstants.COMMAND_ACTION>();
	static {
		for (COMMAND_ACTION ca : COMMAND_ACTION.values()) {
			ACTION_MAPPINGS.put(ca.cmd, ca);
		}
		DEVICE_TYPE.add(ApplicationConstants.COMPUTER);
		DEVICE_TYPE.add(ApplicationConstants.REPEATER);
		DEVICE_TYPE.add(ApplicationConstants.BRIDGE);
	}
}
