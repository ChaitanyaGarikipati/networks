package networks.constants;

public class ErrorConstants {
	public static final String BASE = "Error: ";
	public static final String INVALID_SYNTAX = BASE + "Invalid command syntax.";
	public static final String NODE_NOT_FOUND = BASE + "Node not found.";
	public static final String NAME_EXISTS = BASE + "Devices are already connected.";
	public static final String CONNECT_TO_ITSELF = BASE + "Cannot connect device to itself.";
	public static final String ALREADY_CONNECTED = BASE + "Devices are already connected.";
	public static final String ROUTE_NOT_FOUND = BASE + "Route not found!";
	public static final String ROUTE_TO_REPEATER = BASE + "Route cannot be calculated with a repeater.";
	public static final String COMMAND_NOT_FOUND = BASE + "Command Not Found.";
	public static final String MAP_ENTRY_NOT_FOUND = "Requested Entry Not Found in map/graph";
}
