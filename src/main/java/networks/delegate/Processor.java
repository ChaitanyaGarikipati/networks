package networks.delegate;

import java.util.List;
import java.util.Map;

import networks.beans.Device;
import networks.constants.ApplicationConstants;
import networks.constants.ApplicationConstants.COMMAND_ACTION;
import networks.constants.ErrorConstants;
import networks.service.impl.AddDevice;
import networks.service.impl.ConnectDevices;
import networks.service.impl.FindRoute;
import networks.service.impl.Send;
import networks.service.impl.SetStrength;
import networks.service.inf.Action;

public class Processor {
	public void process(String cmd, Map<Device, List<Device>> networkGraph) {
		String actionCmd = cmd.split(ApplicationConstants.SPACE_STRING)[0];
		Action action = null;
		COMMAND_ACTION actionMappingCmd = ApplicationConstants.ACTION_MAPPINGS.getOrDefault(actionCmd,
				COMMAND_ACTION.INVALID_COMMAND);

		switch (actionMappingCmd) {
		case ADD:
			action = new AddDevice();
			break;

		case SET_STRENGTH:
			action = new SetStrength();
			break;

		case CONNECT:
			action = new ConnectDevices();
			break;

		case FIND_ROUTE:
			action = new FindRoute();
			break;
		
		case SEND_MSG:
			action = new Send();
			break;

		default:
			System.out.println(ErrorConstants.COMMAND_NOT_FOUND);
			break;
		}
		if (action != null)
			action.execute(cmd, networkGraph);
	}
}
