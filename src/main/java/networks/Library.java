package networks;

import java.util.Scanner;

import networks.beans.Network;
import networks.delegate.Processor;

public class Library {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Network network = new Network();
		while (true) {
			String cmd = sc.nextLine();
			Processor processor = new Processor();
			processor.process(cmd, network.getNetworkGraph());
		}
	}
}