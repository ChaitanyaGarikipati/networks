package networks.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Network {
	private Map<Device, List<Device>> networkGraph;

	public Network() {
		networkGraph = new HashMap<Device, List<Device>>();
	}

	public Map<Device, List<Device>> getNetworkGraph() {
		return networkGraph;
	}
}
