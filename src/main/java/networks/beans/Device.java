package networks.beans;

public class Device {
	protected final String name;
	protected int strength;
	protected String type;

	public Device(String name, String type) {
		super();
		this.name = name;
		this.type = type;
		this.strength = 5;
	}
	
	public Device(String name) {
		super();
		this.name = name;
		this.strength = 5;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}
	
	public int updateStrength(int strength) {
		return strength;
	}
	
	public String processMsg(String msg) {
		return msg;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		Device other = (Device) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Device [name=" + name + ", strength=" + strength + ", type=" + type + "]";
	}
}
